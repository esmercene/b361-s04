from django.urls import path

from . import views

'''
	path()
		Syntax:
			path(route, view, name)
'''
urlpatterns = [
	# /todoitem route
	path('', views.index, name='index'),
	# /todoitem/<todoitem_id> route
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
]
