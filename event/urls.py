from django.urls import path

from . import views


app_name = 'event'
urlpatterns = [

	path('', views.index, name='index'),
	# /todoitem/<todoitem_id> route
	path('eventitem<int:eventitem_id>/', views.eventitem, name='eventitem'),
	path('add_event/', views.add_event, name='add_event'),
	path('<int:eventitem_id>/edit', views.update_event, name='update_event'),
	path('<int:eventitem_id>/delete', views.delete_event, name='delete_event'),
]