from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password
from django.contrib import messages


# Local imports
from .models import Event
from .forms import  AddEventForm, UpdateEventForm


def index(request):
	eventitem_list = Event.objects.filter(user_id=request.user.id)
	context = {'eventitem_list': eventitem_list}
	return render(request, "event/index.html", context)




def eventitem(request, eventitem_id):
	# retrieves the item object using the id or primary key
	eventitem = get_object_or_404(Event, pk=eventitem_id)


	return render(request, "event/eventitem.html", model_to_dict(eventitem))


def add_event(request):
    context = {}
    
    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid():
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            # Check the database if an event with the same name already exists
            duplicates = Event.objects.filter(event_name=event_name)

            # If the event does not contain any duplicates, create it
            if not duplicates:
                event = Event.objects.create(event_name=event_name, description=description, event_date=timezone.now(), user_id=request.user.id)
                event_id = event.id  # Get the newly created event's ID
                return redirect("event:eventitem", eventitem_id=event_id)  # Pass event_id as an argument
            else:
                context = {
                    "error" : True
                }
        else:
            # Pass the form with errors to the template
            context['form'] = form
    
    else:
        form = AddEventForm()
        context['form'] = form

    return render(request, 'event/add_event.html', context)






def update_event(request, eventitem_id):

	eventitem = Event.objects.filter(pk=eventitem_id)

	context = {
		"user" : request.user,
		"eventitem_id" : eventitem_id,
		"event_name" : eventitem[0].event_name,
		"description" : eventitem[0].description,
		"status" : eventitem[0].status
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()
		else: 
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if eventitem:
				eventitem[0].event_name = event_name
				eventitem[0].description = description
				eventitem[0].status = status

				eventitem[0].save()
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}
	return render(request, 'event/update_event.html', context)


def delete_event(request, eventitem_id):
	eventitem = Event.objects.filter(pk=eventitem_id).delete()
	return redirect('todolist:index')






































