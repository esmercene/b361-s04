from django import forms




class AddEventForm(forms.Form):
	event_name = forms.CharField(label='Event Name', max_length=100)
	description = forms.CharField(label='Description', max_length=200)


class UpdateEventForm(forms.Form):
	event_name = forms.CharField(label='Task Name', max_length=100)
	description = forms.CharField(label='Description', max_length=200)
	status = forms.CharField(label='Status', max_length=50)