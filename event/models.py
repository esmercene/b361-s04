from django.db import models
from django.contrib.auth.models import User



# Create your models here.
class Event(models.Model):
    event_name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="pending")
    event_date = models.DateTimeField('event date')
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")