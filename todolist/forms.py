from django import forms
from django.contrib.auth.models import User 


class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	password = forms.CharField(label='Password', max_length=20)



class AddTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)


class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=20)
	status = forms.CharField(label='Status', max_length=50)


class RegisterForm(forms.Form):
    username = forms.CharField(label='Username', max_length=20)
    first_name = forms.CharField(label='First Name', max_length=30)
    last_name = forms.CharField(label='Last Name', max_length=30)
    email = forms.EmailField(label='Email')
    password = forms.CharField(label='Password', widget=forms.PasswordInput())
    confirm_password = forms.CharField(label='Confirm Password', widget=forms.PasswordInput())




class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']
