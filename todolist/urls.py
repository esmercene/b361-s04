from django.urls import path

from . import views

'''
	path()
		Syntax:
			path(route, view, name)
'''


app_name = 'todolist'
urlpatterns = [
	# /todoitem route
	path('', views.index, name='index'),
	# /todoitem/<todoitem_id> route
	path('todoitem<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
	path('register/', views.register, name='register'),
	path('change_password/', views.change_password, name='change_password'),
	path('login/', views.login_view, name='login'),
	path('logout/', views.logout_view, name='logout'),
	path('add_task/', views.add_task, name='add_task'),
	path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
	path('<int:todoitem_id>/delete', views.delete_task, name='delete_task'),
	path('update_user/', views.update_user, name='update_user'),
]