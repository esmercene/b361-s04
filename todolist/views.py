from django.shortcuts import render, redirect, get_object_or_404
# from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password
from django.contrib import messages
from django.contrib.auth.decorators import login_required


# Local imports
from .models import ToDoItem
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, UpdateUserForm


# Create your views here.
def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	context = {'todoitem_list': todoitem_list}
	return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
	# retrieves the item object using the id or primary key
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

	# response = "You are viewing the details of %s"
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            # Check if a user with the same username already exists
            if User.objects.filter(username=username).exists():
                messages.error(request, 'A user with the same username already exists.')
            else:
                # Hash the password using make_password
                hashed_password = make_password(password)

                user_to_register = User.objects.create_user(
                    username=username,
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    password=hashed_password,
                )
                user_to_register.is_staff = False
                user_to_register.is_active = True
                user_to_register.save()
                messages.success(request, 'User successfully registered.')

                return redirect('todolist:index')
    else:
        form = RegisterForm()

    return render(request, "todolist/register.html", {"form": form})





def change_password(request):

	is_user_authenticated = False
	user = authenticate(username = "johndoe", password = "john123")
	print(user)


	# Code here executes if the user is successfully authenticated
	if user is not None:
		authenticated_user = User.objects.get(username = "johndoe")
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True


	context = {
		"is_user_authenticated" : is_user_authenticated
	}

	return render(request, "todolist/change_password.html", context)


def login_view(request):
   
    context = {}


    if request.method == 'POST':

    	form = LoginForm(request.POST)

    	if form.is_valid() == False:
    		# Returns a blank login form
    		form = LoginForm()
    	else:
    		# Retrieves the information from the form
    		username = form.cleaned_data['username']
    		password = form.cleaned_data['password']
    		user = authenticate(username=username, password=password)
    		context = {
    			"username" : username,
    			"password" : password
    		}

    	if user is not None:
    		# Saves the user's ID in the session using Django's session framework
    		login(request, user)
    		return redirect('todolist:index')
    	else:
    		# Provides context with error to conditionally render the error message
    		context = {
    			"error" : True
    		}

    return render(request, 'todolist/login.html', context)	

def logout_view(request):
	# removes the saved data in session upon login
    logout(request)
    return redirect("todolist:index")



def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			# Checks the database if a task is already exists
			duplicates = ToDoItem.objects.filter(task_name=task_name)

			#if todoitem does not contain any duplicates
			if not duplicates:

				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)

			else:
				context = {
					"error" : True
				}
	return render(request, 'todolist/add_task.html', context)


def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user" : request.user,
		"todoitem_id" : todoitem_id,
		"task_name" : todoitem[0].task_name,
		"description" : todoitem[0].description,
		"status" : todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()
		else: 
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}
	return render(request, 'todolist/update_task.html', context)



def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
	return redirect('todolist:index')



def update_user(request):
    if request.method == 'POST':
        form = UpdateUserForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('todolist:index')
    else:
        form = UpdateUserForm(instance=request.user)

    return render(request, 'todolist/update_user.html', {'form': form})



































